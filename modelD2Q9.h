#define TEST 	printf("OK!\n\n")
#define MODE_C

/*#define Tile_X 4
#define Tile_Y 4
*/
#define Q   9

#define UX  0.00F
#define UY  0.00F
/* #define R0  1.000035F */
#define E   6e-6
/*
#define Radius 28.00F
#define X0     280.00F
#define Y0     280.00F
*/
/* Coeff pour penalisation volumique */
#define eta 1e-6

#define V_MAX 0.5F


#define S1  1.1F
#define S2  1.25F
#define S4  1.8F
#define S6 1.8F



#define e1  1
#define e2  eY
#define e3  -1
#define e4  (- eY)
#define e5  (1 + eY)
#define e6  (-1 + eY)
#define e7  (-1 - eY)
#define e8  (1 - eY)




typedef struct vector vector;

struct vector
{
    float x;
    float y;
};

typedef struct model model;

struct model
{
    int dev;
    char* name;
    int length;
    word header[13];



    int lX;
    int lY;
    int Tile_X;
    int Tile_Y;
    int sY;
    int sS; //the total number of the solid neuds.
    int sL; //the total number of the empty neuds. sL = sZ - sS




    int T;  // "Number of time step "
    int P;
    int TP; //total time step

    //float Re;
    float nu;
    float nu2;
    float rho;
    float rho2;
    float f0; //flux volumique entree
    float f1; //flux volumique sortie
//    float avgu; //average velocity
//    float avgu1;
//    float davgu;

//    float Radius;
//    float X0;
//    float Y0;

//    float Masse;
//    float Raideur;
//    float Xeq;
//    float Yeq;

  /* for device */
    float* ui;  // vitesse entree
    float* l0;    // densite de probabilite 0
    float* l1;    // densite de probabilite 1
    vector* v;    // champ de vitesse macro
    float* r;   // masse volumique
    float* pr; // pression macroscopique
    vector* fi; // force NS (dans le domaine solide)
    int* lf; //fonction caracteristique
    float* coefdev;  // pouur calculer coef hydro et energie
    //vector *vs; // vitesse dans le domaine solide
//    float *umo; //vitesse moyenne
//    float *umold; // vitesse moyenne

//
//    float* strudev ; // caracteristique structure Xcentre, Ycentre,Thetacentre VScentre, VYcentre,Wcentre
//    byte* bv;  //je ne sais pas


  /* for host */
    vector* u0;  // vitesse macro
//    vector* u1;
    float* uinlet;  // vitesse entree
//    vector* force;  // force macro
//    byte* bmp_v;
//    byte* bmp_t;
//    float Du;
    float* rh; // masse volumique ou pression bref un champ scalaire
    int*  lf0; // fonction caracteristique
    float* l0h; // densite de probabilite
    float* coefhost; // pouur calculer coef hydro et energie
//    vector* ur;
//    vector* su2;
//    float* coefhost;
//    float* struhost ; // caracteristique structure Xcentre, Ycentre,Thetacentre VScentre, VYcentre,Wcentre
    //float* energy;
    FILE* FileE; //sauvegardes
    FILE* FileS; // Sauvegarde cara solide rigide
};
