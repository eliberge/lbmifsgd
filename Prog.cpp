#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include "memory.h"
#include "modelD2Q9.h"
#include "init.h"
#include "lbmgpu.h"
#include "ecriture.h"

// CUDA Runtime
#include <cuda_runtime.h>

// Utilities and system includes
#include <helper_cuda.h>
#include <helper_functions.h>
#include <algorithm>

void print_cur_time(const char* l)
{
  time_t lt;
  struct tm *lp;

  time(&lt);
  lp = localtime(&lt);
  printf("%s: %s", l, asctime(lp));
}

float elapsed_time(struct timespec* s, struct timespec* t)
{
    return (float)(t->tv_sec - s->tv_sec)
	   + (float)(t->tv_nsec - s->tv_nsec)*1e-9;
}

int main(int argc, char **argv)
{
  int i,j;
  model M;
  struct timespec s, t;
  float T, P;
  int dev;
  //FILE* fileE;
  cudaDeviceProp deviceProp;


  dev = findCudaDevice(argc, (const char **)argv);
  dev = 0 ;
  checkCudaErrors(cudaGetDeviceProperties(&deviceProp, dev));

  printf("Using Device %d: %s\n\n", dev, deviceProp.name);
  checkCudaErrors(cudaSetDevice(dev));

  get_param(argc, argv, &M,dev);
  //# Initialisation du model

  ///////////////////////////////////////////////////////////
  init_model( &M);
  print_cur_time("Begin at");
  clock_gettime(CLOCK_REALTIME, &s);
  for (i = 0; i < M.T ; i++)
    {
      compute_tlbm(&M);
      compute_results(&M);
      ecriture_vtk(&M, 2*M.P*(i+1)+M.TP);
    }


    clock_gettime(CLOCK_REALTIME, &t);
    T = elapsed_time(&s, &t);
    printf("Elapsed time: %.4g s\n", T);

    write_stat2D(&M);
    free_model(&M);
}
