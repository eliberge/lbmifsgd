#include <stdio.h>
#include "memory.h"
#include "modelD2Q9.h"
#include "ecriture.h"

__constant__ float R1;
__constant__ float R2;
__constant__ int lX;
__constant__ int lY;
__constant__ int Tile_X;
__constant__ int Tile_Y;
__constant__ int sY;
__constant__ int mX;
__constant__ int mY;
__constant__ int eY;

__constant__ float i0;
__constant__ float i1;
__constant__ float i2;

__constant__ float s1;
__constant__ float s2;
__constant__ float s4;
__constant__ float s7;
__constant__ float s72;

__constant__ float c0;
__constant__ float c1;
__constant__ float c2;
__constant__ float c3;
__constant__ float c4;
__constant__ float c5;

#define _(n) (n)*lX
////////////////////////////////////////////////////////////////
////////// Launchers //////////

__device__ int index(int x, int y)
{
    return x + y*eY;
}
/////////////////////////////////////////////////////////////////////
__global__ void initUetP(vector* v,float* pr, vector* Fv, float *ro, int* fcar)
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y +threadIdx.y;
      //    int offset = x + y*lX + z*sY;
    /* float tau; */
  int offset = x + y*lX ;
  v += offset;
  pr += offset;
  Fv +=offset;
  ro +=offset;
  fcar +=offset;
  v->x = 0.0 ; //-0.02*3.14159*(y-128)/256.0;
  v->y = 0.0; //0.02*3.14159*(x-128)/256.0; // 0.01*sin(3.14159*y/mY); //0.0;
  *pr= 0.0;
  Fv->x = 0.0 ; //-0.02*3.14159*(y-128)/256.0;
  Fv->y = 0.0; //0.02*3.14159*(x-128)/256.0; // 0.01*sin(3.14159*y/mY); //0.0;
  *ro  =((*fcar) -(-1.0))/(1.0-(-1.0))*(R1-R2)+R2;
}
/////////////////////////////////////////////////////////////////////
__global__ void initD2Q9(float* l, vector* v, float *ro)
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y +threadIdx.y;
  int offset = x + y*lX ;
    /* float tau; */
  v += offset;
  ro +=offset;
  l += index(x, y);


    //
    l[0]     = i0*(*ro)*(1.-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
    l[_(1)]  = i1*(*ro)*(1+3*(v->x)+(9./2.)*((v->x)*(v->x))-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
    l[_(2)]  = i1*(*ro)*(1+3*(v->y)+(9./2.)*((v->y)*(v->y))-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
    l[_(3)]  = i1*(*ro)*(1-3*(v->x)+(9./2.)*((v->x)*(v->x))-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
    l[_(4)]  = i1*(*ro)*(1-3*(v->y)+(9./2.)*((v->y)*(v->y))-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
    l[_(5)]  = i2*(*ro)*(1+3*((v->x)+(v->y))+9.*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
    l[_(6)]  = i2*(*ro)*(1-3*((v->x)-(v->y))-9.*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
    l[_(7)]  = i2*(*ro)*(1-3*((v->x)+(v->y))+9.*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
    l[_(8)]  = i2*(*ro)*(1+3*((v->x)-(v->y))-9.*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );


}
/////////////////////////////////////////////////////////////////////
__global__ void results(float* s, float* d,vector* v, float* r0, int* t,float* ui, vector* Fs)
{
    //int x = threadIdx.x;
    //int y =  threadIdx.y;
    int x = blockIdx.x*blockDim.x + threadIdx.x;
    int y = blockIdx.y*blockDim.y +threadIdx.y;
    int offset = x + y*lX ;
    //float dux,duy;
    //float energyloc;
    //int ind;
    s += index(x, y);
    d += index(x, y);
    v += offset;
    r0 += offset;
    t += offset;
    Fs += offset;
    // fs +=offset
    ui += x;

    float f0, f1, f2, f3, f4, f5, f6;
    float f7, f8;

    if (x > 0 && x < mX  && y >0 && y < mY)
     {
    // Reading and propagation

    f0  = s[0];
    f1  = s[_(1)  - e1];
    f2  = s[_(2)  - e2];
    f3  = s[_(3)  - e3];
    f4  = s[_(4)  - e4];
    f5  = s[_(5)  - e5];
    f6  = s[_(6)  - e6];
    f7  = s[_(7)  - e7];
    f8  = s[_(8)  - e8];

     }
    /*M->sY = M->lX*M->lY; M->sZ = M->lX*M->lY*M->lZ;int _mX = M->lX - 1;  int _mY = M->lY - 1;  int _mZ = M->lZ - 1;  int _eY = M->lX*Q;  int _eZ = M->sY*Q;  int _v0 = 4*M->lX, now changed to 4*(M->lX+2);  int _vY = M->lX; int _vZ = 3*M->lX;*/
    /* CL pour cavite*/
    else if (x == 0 )
    {
      f0  = s[0];
      f1 =s[_(3)];
      f5 =s[_(7)];
      f8 =s[_(6)];
      if(y == 0)
      {
        f2 = s[_(4)];
        f3  = s[_(3)  - e3];
        f4  = s[_(4)  - e4];
        f6 = s[_(8)];
        f7  = s[_(7)  - e7];
      }
      else if ( y == mY )
      {
        f2  = s[_(2)  - e2];
        f3  = s[_(3)  - e3];
        f4 = s[_(2)];
        f6  = s[_(6)  - e6];
        f7  = s[_(5)];
      }
      else
      {
        f2  = s[_(2)  - e2];
        f3  = s[_(3)  - e3];
        f4  = s[_(4)  - e4];
        f6  = s[_(6)  - e6];
        f7  = s[_(7)  - e7];
       }
     }
     else if (y == 0 && x > 0 && x < mX)
     {
       f0  = s[0];
       f1  = s[_(1)  - e1];
       f3  = s[_(3)  - e3];
       f4  = s[_(4)  - e4];
       f7  = s[_(7)  - e7];
       f8  = s[_(8)  - e8];
       f2 = s[_(4)];
       f5 = s[_(7)];
       f6 = s[_(8)];
      }
      else if (y == mY && x > 0 && x < mX)
      {
        f0  = s[0];
        f1  = s[_(1)  - e1];
        f2  = s[_(2)  - e2];
        f3  = s[_(3)  - e3];
        f5  = s[_(5)  - e5];
        f6  = s[_(6)  - e6];
        f4 = s[_(2)];
        f7 = s[_(5)]-(1./6)*(*r0)*(*ui);
        f8 = s[_(6)]+(1./6)*(*r0)*(*ui);
      }
      else if (x == mX)
      {
        f0  = s[0];
        f1  = s[_(1)  - e1];
        f3 =s[_(1)];
        f6 =s[_(8)];
        f7 =s[_(5)];
        if (y==0)
        {
          f2  = s[_(4)];
          f4  = s[_(4)  - e4];
          f5  = s[_(7)];
          f8  = s[_(8)  - e8];
        }
        else if ( y == mY )
        {
          f2  = s[_(2)  - e2];
          f5  = s[_(5)  - e5];
          f4  = s[_(2)];
          f7  = s[_(5)];
          f8  = s[_(6)] ;
         }
         else
         {
          f2  = s[_(2)  - e2];
          f4  = s[_(4)  - e4];
          f5  = s[_(5)  - e5];
          f8  = s[_(8)  - e8];
         }
        }

/*
    // Boundary conditions 1 : Bounce back, impermeable walls, constants pressure on x=0 and x=mX
    if (x == 0 )
   {
     f0  = s[0];
     f1 =s[_(3)]+(2.*1./3)*(*ui);
     f5 =s[_(7)]+(2.*1./12)*(*ui);
     f8 =s[_(6)]+(2.*1./12)*(*ui);
     if(y == 0)
     {
       f2 = s[_(4)];
       f3  = s[_(3)  - e3];
       f4  = s[_(4)  - e4];
       f6 = s[_(7)];
       f7  = s[_(7)  - e7];
     }
     else if ( y == mY )
     {
       f2  = s[_(2)  - e2];
       f3  = s[_(3)  - e3];
       f4 = s[_(2)];
       f6  = s[_(6)  - e6];
       f7  = s[_(6) ];

     }
     else
     {
       f2  = s[_(2)  - e2];
       f3  = s[_(3)  - e3];
       f4  = s[_(4)  - e4];
       f6  = s[_(6)  - e6];
       f7  = s[_(7)  - e7];
     }

   }

   if (y == 0 && x > 0 && x < mX) {
       f0  = s[0];
       f1  = s[_(1)  - e1];
       f2 = s[_(4)];
       f3  = s[_(3)  - e3];
       f4  = s[_(4)  - e4];
       f5 = s[_(8)];
       f6 = s[_(7)];
       f7  = s[_(7)  - e7];
       f8  = s[_(8)  - e8];

     }


     if (y == mY && x > 0 && x < mX) {
       f0  = s[0];
       f1  = s[_(1)  - e1];
       f2  = s[_(2)  - e2];
       f3  = s[_(3)  - e3];
       f4 = s[_(2)];
       f5  = s[_(5)  - e5];
       f6  = s[_(6)  - e6];
       f7 = s[_(6)];
       f8 = s[_(5)];

     }

     if (x == mX)
      {
       f0  = s[0];
       f1  = s[_(1)  - e1];
        // dux=-(uinitx)*(3*v->x -4*v[-1].x +v[-2].x )/2.;
       // duy=-(uinitx)*(3*v->y -4*v[-1].y +v[-2].y )/2.;
       dux=-(*umo1)*(3*v->x -4*v[-1].x +v[-2].x )/2.;
       duy=-(*umo1)*(3*v->y -4*v[-1].y +v[-2].y )/2.;
       f3 =d[_(3)]-(3./9)*dux;
       f6 =d[_(6)]+(3./36)*(-dux+duy);
       f7 =d[_(7)]+(3./36)*(-dux-duy);
       atomicAdd(umo2,(v->x)/(mY+1));
        if(y == 0)
        {
          f2 = s[_(4)];
          f4 = s[_(4) - e4] ;
          f5  = s[_(8)];
          f8 = s[_(8) - e8] ;
        }
        else if ( y == mY )
        {
          f2 =s[_(2) - e2];
          f4 =s[_(2)];
          f5 =s[_(5) - e5];
          f8 =s[_(5)];

        }
        else
        {
          f2 =s[_(2) - e2];
          f4 = s[_(4) - e4] ;
          f5 =s[_(5) - e5] ;
          f8 =s[_(8) - e8] ;
        }

     }
*/

    __syncthreads();



    // Calcul de densite et vitesse
    *r0  =f0 + f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8 ;
    v->x = ((f1 - f3 + f5 - f6 - f7 + f8 + (Fs->x)/2)/(*r0) ) ;
    v->y = ((f2 - f4 + f5 + f6 - f7 - f8 + (Fs->y)/2)/(*r0));
    //energyloc=(v->x)*(v->x)+(v->y)*(v->y);
    //atomicAdd(&ENERGY[0],energyloc);

}
/////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__global__ void initf(float* l,float* pr, vector* vs)
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y +threadIdx.y;
      //    int offset = x + y*lX + z*sY;
    /* float tau; */
  int offset = x + y*lX ;
  //float rloc;
  l += index(x, y);
  pr += offset;
  vs += offset;
  //rloc=((*lf) -(-1.0))/(1.0-(-1.0))*(R1-R2)+R2;
    //
    l[0]     = -(1. - i0)*(*pr)*3 - i0*(3./2.)*((vs->x)*(vs->x) + (vs->y)*(vs->y));
    l[_(1)]  = i1*((*pr)*3+3*(vs->x)+(9./2)*((vs->x)*(vs->x))-(3./2.)*((vs->x)*(vs->x) + (vs->y)*(vs->y)) );
    l[_(2)]  = i1*((*pr)*3+3*(vs->y)+(9./2)*((vs->y)*(vs->y))-(3./2.)*((vs->x)*(vs->x) + (vs->y)*(vs->y)) );
    l[_(3)]  = i1*((*pr)*3-3*(vs->x)+(9./2)*((vs->x)*(vs->x))-(3./2.)*((vs->x)*(vs->x) + (vs->y)*(vs->y)) );
    l[_(4)]  = i1*((*pr)*3-3*(vs->y)+(9./2)*((vs->y)*(vs->y))-(3./2.)*((vs->x)*(vs->x) + (vs->y)*(vs->y)) );
    l[_(5)]  = i2*((*pr)*3+3*((vs->x)+(vs->y))+(9.)*((vs->x)*(vs->y))+3.*((vs->x)*(vs->x)+(vs->y)*(vs->y)) );
    l[_(6)]  = i2*((*pr)*3+3*(-(vs->x)+(vs->y))-(9.)*((vs->x)*(vs->y))+3.*((vs->x)*(vs->x)+(vs->y)*(vs->y)) );
    l[_(7)]  = i2*((*pr)*3+3*(-(vs->x)-(vs->y))+(9.)*((vs->x)*(vs->y))+3.*((vs->x)*(vs->x)+(vs->y)*(vs->y)) );
    l[_(8)]  = i2*((*pr)*3+3*((vs->x)-(vs->y))-(9.)*((vs->x)*(vs->y))+3.*((vs->x)*(vs->x)+(vs->y)*(vs->y)) );


}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
__global__ void ComputeMacroInit(float* s, vector* v, float* r0, int* fcar, float* pr)
{
    //int x = threadIdx.x;
    //int y =  threadIdx.y;
    int x = blockIdx.x*blockDim.x + threadIdx.x;
    int y = blockIdx.y*blockDim.y +threadIdx.y;
    int offset = x + y*lX ;
    //float dux,duy;
    //float energyloc;
    s += index(x, y);
    //d += index(x, y);
    v += offset;
    r0 += offset;
    fcar += offset;
    pr += offset;

    float f1, f2, f3, f4, f5, f6;
    float f7, f8;


    //f0  = s[0];
    f1  = s[_(1)];
    f2  = s[_(2)];
    f3  = s[_(3)];
    f4  = s[_(4)];
    f5  = s[_(5)];
    f6  = s[_(6)];
    f7  = s[_(7)];
    f8  = s[_(8)];

    //__syncthreads();

    //if (threadIdx.x == 0) *umo1=*umo2 ;

    // Calcul de densite et vitesse
    *r0  =((*fcar) -(-1.0))/(1.0-(-1.0))*(R1-R2)+R2;
    v->x =(f1 - f3 + f5 - f6 - f7 + f8 )/(2*(*r0)) ; //+ (*t)*(Fs->x)/(2))/(*r0) ;
    v->y = (f2 - f4 + f5 + f6 - f7 - f8 )/(2*(*r0)) ; // + (*t)*(Fs->y)/(2))/(*r0) ;
    *pr= ((1./3)*(f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8)-i0*((v->x)*(v->x) + (v->y)*(v->y) )/2.)/(1.-i0);
   /*  energyloc=(v->x)*(v->x)+(v->y)*(v->y);
    atomicAdd(&ENERGY[0],energyloc);*/
    // *r0  =f3  ;
    // v->x =f4 ;
    // v->y =f5 ;
    // if (y == 0 )
    // {
    //	*r0=0.5;
    //   v->x = 0;
    //   v->y = 0;
    //
    // }



}
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
__global__ void Prediction(float* s,float* d, vector* v, vector* Fs,float* r0, int* t,float* ui, float* pr)
{
    //int x = threadIdx.x;
    //int y =  threadIdx.y;
    int x = blockIdx.x*blockDim.x + threadIdx.x;
    int y = blockIdx.y*blockDim.y +threadIdx.y;
    int offset = x + y*lX ;
    //float dux,duy;
    //float energyloc;
    int indloc;
    float faeq,fabeq;
    s += index(x, y);
    d += index(x, y);
    //d += index(x, y);
    v += offset;
    r0 += offset;
    t += offset;
    Fs += offset;
    pr += offset;
    // fs +=offset
    ui += x;

    float f0, f1, f2, f3, f4, f5, f6;
    float f7, f8;
    float fx,fy;
    fx=Fs->x;
    fy=Fs->y;

    if (x > 0 && x < mX  && y >0 && y < mY)
    {
    f0  = s[0];
    f1  = s[_(1)  - e1];
    f2  = s[_(2)  - e2];
    f3  = s[_(3)  - e3];
    f4  = s[_(4)  - e4];
    f5  = s[_(5)  - e5];
    f6  = s[_(6)  - e6];
    f7  = s[_(7)  - e7];
    f8  = s[_(8)  - e8];

    }
    else  if (y == 0 && x > 0 && x < mX)
   {
    // No slip Bottom
    f0  = s[0];
    f1  = s[_(1)  - e1];
    f3  = s[_(3)  - e3];
    f4  = s[_(4)  - e4];
    f7  = s[_(7)  - e7];
    f8  = s[_(8)  - e8];

    f2 = f4 + i1*3*(-fy)/(*r0);
    f5 = f7+ i2*3*(-fx-fy)/(*r0);
    f6 = f8+ i2*3*(fx-fy)/(*r0);

    }
    else  if (y == mY && x > 0 && x < mX)
   {
     // imposer vitesse en haut
    f0  = s[0];
    f1  = s[_(1)  - e1];
    f2  = s[_(2)  - e2];
    f3  = s[_(3)  - e3];
    f5  = s[_(5)  - e5];
    f6  = s[_(6)  - e6];
    faeq= i1*((*pr)*3-3*(0)+(9./2)*((0)*(0))-(3./2.)*((*ui)*(*ui) + (0)*(0)) );
    fabeq=i1*((*pr)*3+3*(0)+(9./2)*((0)*(0))-(3./2.)*((*ui)*(*ui) + (0)*(0)) );
    f4 = f2 +(faeq-fabeq);// s[_(2)];//f2;
    faeq= i2*((*pr)*3+3*((*ui)-(0))-(9.)*((*ui)*(0))+3.*((*ui)*(*ui)+(0)*(0)) );
    fabeq= i2*((*pr)*3+3*(-(*ui)+(0))-(9.)*((*ui)*(0))+3.*((*ui)*(*ui)+(0)*(0)) );
    f8 = f6 +(faeq-fabeq);
    faeq= i2*((*pr)*3+3*(-(*ui)-(0))+(9.)*((*ui)*(0))+3.*((*ui)*(*ui)+(0)*(0)) );
    fabeq=i2*((*pr)*3+3*((*ui)+(0))+(9.)*((*ui)*(0))+3.*((*ui)*(*ui)+(0)*(0)) );
    f7 = f5 +(faeq-fabeq);

    faeq=  i1*((*pr)*3+3*(*ui)+(9./2)*((*ui)*(*ui))-(3./2.)*((*ui)*(*ui) + (0)*(0)) );
    fabeq= i1*((*pr)*3-3*(*ui)+(9./2)*((*ui)*(*ui))-(3./2.)*((*ui)*(*ui) + (0)*(0)) );
    f8 += -(1./2)*((f1-faeq) -(f3-fabeq) ) ;// s[_(6)];//f6 -(f1-f3)/2;
    f7 += -(1./2)*(-(f1-faeq) +(f3-fabeq) ) ;// s[_(6)];//f6 -(f1-f3)/2;
     // s[_(5)];// f5 + (f1-f3)/2 ;
    /*
      f4 =  2*s[_(4)+e4 ]- s[_(4)+2*e4]; // s[_(2)];//f2;
      f8 =  2*s[_(8)+e4 ]- s[_(8)+2*e4]; //  s[_(6)];//f6 -(f1-f3)/2;
      f7 =  2*s[_(7)+e4 ]- s[_(7)+2*e4]; //  s[_(5)];// f5 + (f1-f3)/2 ;
      */
   }
   else if (x == 0 )
   {
    f0  = s[0];
    indloc = _(1) + lX-1;
    f1 =s[indloc];

    if(y == 0)
    {

      f3  = s[_(3)  - e3];
      f4  = s[_(4)  - e4];
      f7  = s[_(7)  - e7];
      indloc = _(8) + eY+(lX-1);
      f8 =s[indloc];

      f2 = f4 + i1*3*(-fy)/(*r0); // s[_(4)];//f4;
      f5 = f7 + i2*3*(-fx-fy)/(*r0);// s[_(7)]; //f7 - (f1-f3)/2.;
      f6 = f8 + i2*3*(fx-fy)/(*r0); // s[_(8)];//f8 + (f1-f3)/2.;
      /*
      f2 = 2*s[_(2)+e2 ]- s[_(2)+2*e2]; // s[_(4)];//f4;
      f5 = 2*s[_(5)+e2 ]- s[_(5)+2*e2]; // s[_(7)]; //f7 - (f1-f3)/2.;
      f6 = 2*s[_(6)+e2 ]- s[_(6)+2*e2]; // s[_(8)];//f8 + (f1-f3)/2.;
      */

    }
    else if ( y == mY )
    {
      f2  = s[_(2)  - e2];
      f3  = s[_(3)  - e3];
      f6  = s[_(6)  - e6];
      indloc = _(5) -eY+(lX-1);
      f5 = s[indloc];
      f4 = f2 + i1*3*(fy)/(*r0);// s[_(2)];//f2;
      f8 = f6 + i2*3*(-fx+fy)/(*r0) ; // s[_(6)];//f6 -(f1-f3)/2;
      f7 = f5 + i2*3*(+fx+fy)/(*r0); // s[_(5)];// f5 + (f1-f3)/2 ;
      /*
        f4 =  2*s[_(4)+e4 ]- s[_(4)+2*e4]; // s[_(2)];//f2;
        f8 =  2*s[_(8)+e4 ]- s[_(8)+2*e4]; //  s[_(6)];//f6 -(f1-f3)/2;
        f7 =  2*s[_(7)+e4 ]- s[_(7)+2*e4]; //  s[_(5)];// f5 + (f1-f3)/2 ;
        */

    }
    else
    {
      f2  = s[_(2)  - e2];
      f3  = s[_(3)  - e3];
      f4  = s[_(4)  - e4];
      f6  = s[_(6)  - e6];
      f7  = s[_(7)  - e7];

      f5 = f7 + i2*3*(-fx-fy)/(*r0);
      f8 = f6 + i2*3*(-fx+fy)/(*r0) ;
    }


  }
  else if (x == mX)
   {
     f0  = s[0];
     f1  = s[_(1)  - e1];
     indloc = _(3) - (lX-1);
     f3 =s[indloc];

     if (y==0)
     {

       f4  = s[_(4)  - e4];
       f8  = s[_(8)  - e8];
       indloc= _(7) - (lX-1) + eY;
       f7 =s[indloc];
       //f3 = f1;
      // f2 = f4;
      // f5 = f7 - (f1-f3)/2.;
      // f6 = f8 + (f1-f3)/2.;
      f2 = f4 + i1*3*(-fy)/(*r0); // s[_(4)];//f4;
      f5 = f7 + i2*3*(-fx-fy)/(*r0);// s[_(7)]; //f7 - (f1-f3)/2.;
      f6 = f8 + i2*3*(fx-fy)/(*r0); // s[_(8)];//f8 + (f1-f3)/2.;
      /*
      f2 = 2*s[_(2)+e2 ]- s[_(2)+2*e2]; // s[_(4)];//f4;
      f5 = 2*s[_(5)+e2 ]- s[_(5)+2*e2]; // s[_(7)]; //f7 - (f1-f3)/2.;
      f6 = 2*s[_(6)+e2 ]- s[_(6)+2*e2]; // s[_(8)];//f8 + (f1-f3)/2.;
      */


     }
     else if ( y == mY )
     {
       f2  = s[_(2)  - e2];
       f5  = s[_(5)  - e5];
       indloc = _(6) - (lX-1) -eY;
       f6 =s[indloc];
     //   f4  =f2; // s[_(2)];
     //   f7 = f5 + (f1-f3)/2 ; // s[_(5)];
     //   f8 = f6 -(f1-f3)/2;
     f4 = f2 + i1*3*(fy)/(*r0);// s[_(2)];//f2;
     f8 = f6 + i2*3*(-fx+fy)/(*r0) ; // s[_(6)];//f6 -(f1-f3)/2;
     f7 = f5 + i2*3*(+fx+fy)/(*r0); // s[_(5)];// f5 + (f1-f3)/2 ;
     /*
       f4 =  2*s[_(4)+e4 ]- s[_(4)+2*e4]; // s[_(2)];//f2;
       f8 =  2*s[_(8)+e4 ]- s[_(8)+2*e4]; //  s[_(6)];//f6 -(f1-f3)/2;
       f7 =  2*s[_(7)+e4 ]- s[_(7)+2*e4]; //  s[_(5)];// f5 + (f1-f3)/2 ;
       */

     }
     else
     {
        f2  = s[_(2)  - e2];
        f4  = s[_(4)  - e4];
        f5  = s[_(5)  - e5];
        f8  = s[_(8)  - e8];
        f6 =f8 + i2*3*(fx-fy)/(*r0);
        f7 =f5 + i2*3*(fx+fy)/(*r0);

     }
  }
/*
   else if (x == 0 )
   {
     // No slip a gauche
    f0  = s[0];
    f1 =f3+i1*3*(-fx)/(*r0);

    if(y == 0)
    {

      f3  = s[_(3)  - e3];
      f4  = s[_(4)  - e4];
      f7  = s[_(7)  - e7];

      f2 = f4 + i1*3*(-fy)/(*r0); // s[_(4)];//f4;
      // s[_(7)]; //f7 - (f1-f3)/2.;
      f6 = f8 + i2*3*(fx-fy)/(*r0); // s[_(8)];//f8 + (f1-f3)/2.;

      f5 = f7 + i2*3*(-fx-fy)/(*r0);
      f8 =f6+ i2*3*(-fx+fy)/(*r0);
    }
    else if ( y == mY )
    {
      f2  = s[_(2)  - e2];
      f3  = s[_(3)  - e3];
      f6  = s[_(6)  - e6];
      indloc = _(5) -eY+(lX-1);
      f5 = s[indloc];
      f4 = f2 + i1*3*(fy)/(*r0);// s[_(2)];//f2;
      f8 = f6 + i2*3*(-fx+fy)/(*r0) ; // s[_(6)];//f6 -(f1-f3)/2;
      f7 = f5 + i2*3*(+fx+fy)/(*r0); // s[_(5)];// f5 + (f1-f3)/2 ;
    }
    else
    {
      f2  = s[_(2)  - e2];
      f3  = s[_(3)  - e3];
      f4  = s[_(4)  - e4];
      f6  = s[_(6)  - e6];
      f7  = s[_(7)  - e7];
      f5 = f7 + i2*3*(-fx-fy)/(*r0);
      f8 = f6 + i2*3*(-fx+fy)/(*r0) ;
    }
  }
  else if (x == mX)
   {
     f0  = s[0];
     f1  = s[_(1)  - e1];
     indloc = _(3) - (lX-1);
     f3 =f1+i1*3*(fx)/(*r0);
     if (y==0)
     {

       f4  = s[_(4)  - e4];
       f8  = s[_(8)  - e8];
       f2 = f4 + i1*3*(-fy)/(*r0); // s[_(4)];//f4;
       indloc = _(5) +(lY-2)*eY-1;
       f5 = s[indloc];

       f6 = f8 + i2*3*(fx-fy)/(*r0); // s[_(8)];//f8 + (f1-f3)/2.;
       f7 =f5 + i2*3*(fx+fy)/(*r0);
     }
     else if ( y == mY )
     {
       f2  = s[_(2)  - e2];
       f5  = s[_(5)  - e5];

       indloc = _(8) + eY+(lX-1);
       f8 =s[indloc];
       f4 = f2 + i1*3*(fy)/(*r0);// s[_(2)];//f2;
       f7 = f5 + i2*3*(+fx+fy)/(*r0); // s[_(5)];// f5 + (f1-f3)/2 ;
       f6 =f8 + i2*3*(fx-fy)/(*r0);

     }
     else
     {
        f2  = s[_(2)  - e2];
        f4  = s[_(4)  - e4];
        f5  = s[_(5)  - e5];
        f8  = s[_(8)  - e8];
        f6 =f8 + i2*3*(fx-fy)/(*r0);
        indloc= _(7) - (lX-1) + eY;
        f7 =f5 + i2*3*(fx+fy)/(*r0);

     }
  }
*/

    //__syncthreads();

    //if (threadIdx.x == 0) *umo1=*umo2 ;
    *pr= ((1./3)*(f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8)-i0*((v->x)*(v->x) + (v->y)*(v->y) )/2.)/(1.-i0);
    // Calcul de densite et vitesse
    *r0  =((*t) -(-1.0))/(1.0-(-1.0))*(R1-R2)+R2;
    v->x = f1 - f3 + f5 - f6 - f7 + f8 +(Fs->x)/(2*(*r0)) ; //+ (*t)*(Fs->x)/(2))/(*r0) ;
    v->y = f2 - f4 + f5 + f6 - f7 - f8 +(Fs->y)/(2*(*r0)) ; // + (*t)*(Fs->y)/(2))/(*r0) ;

   /*  energyloc=(v->x)*(v->x)+(v->y)*(v->y);
    atomicAdd(&ENERGY[0],energyloc);*/
    // *r0  =f3  ;
    // v->x =f4 ;
    // v->y =f5 ;
    // if (y == 0 )
    // {
    //	*r0=0.5;
    //   v->x = 0;
    //   v->y = 0;
    //
    // }
    d[0]= f0;
    d[_(1)] = f1;
    d[_(2)] = f2;
    d[_(3)] = f3;
    d[_(4)] = f4;
    d[_(5)] = f5;
    d[_(6)] = f6;
    d[_(7)] = f7;
    d[_(8)] = f8;



}
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
__global__ void Correction(float* s, vector* vs, float* pr, vector* Fs, float* r0)
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y +threadIdx.y;
  int offset = x + y*lX ;

  float f1, f2, f3, f4, f5, f6,f7, f8;
  s += index(x, y);
  vs += offset;
  pr += offset;
  Fs += offset;
  r0 += offset;

  //f0  = s[0];
  f1  = s[_(1)];
  f2  = s[_(2)];
  f3  = s[_(3)];
  f4  = s[_(4)];
  f5  = s[_(5)];
  f6  = s[_(6)];
  f7  = s[_(7)];
  f8  = s[_(8)];

  *pr= ((1./3)*(f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8)-i0*((vs->x)*(vs->x) + (vs->y)*(vs->y) )/2.)/(1.-i0);
  // Calcul de densite et vitesse
  //*r0  =((*lf) -(-1.0))/(1.0-(-1.0))*(R1-R2)+R2;
  vs->x = f1 - f3 + f5 - f6 - f7 + f8 +(Fs->x )/(2*(*r0)) ; //+ (*t)*(Fs->x)/(2))/(*r0) ;
  vs->y = f2 - f4 + f5 + f6 - f7 - f8 +(Fs->y )/(2*(*r0)) ; // + (*t)*(Fs->y)/(2))/(*r0) ;

}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
__global__ void lbmTRT(float* s, float* d, float* ui,vector* v, float* r0, vector* Fs,int* phi)
{
  // Utilisation du MRT avec seulement 2 coefs pour TRT
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y +threadIdx.y;
  int offset = x + y*lX ;

  int i = index(x, y);

  v += offset;
  r0 += offset;
  s += i;
  d += i;
  Fs += offset;

  float f0, f1, f2, f3, f4, f5, f6,f7, f8;
  float feq0,feq1,feq2,feq3,feq4,feq5,feq6,feq7,feq8;
  float rho, e, eps, jx, qx, jy, qy,pxx, pxy;
  //float dux,duy;
  float muloc,sp,sm,Lambda=1./6;
  // float tx, ty,tx2,ty2;

  /* float v1, v2; */
  /* float a,b,c,dd,ee,ff; */
  /* float a1,b1,c1,dd1,ee1,ff1; */

  /* float* t = (float *)T + v0 + x; //extern __shared__ float T[]; */


  if (x > 0 && x < mX  && y >0 && y < mY)
  {
  // Reading and propagation

  f0  = s[0];
  f1  = s[_(1)  - e1];
  f2  = s[_(2)  - e2];
  f3  = s[_(3)  - e3];
  f4  = s[_(4)  - e4];
  f5  = s[_(5)  - e5];
  f6  = s[_(6)  - e6];
  f7  = s[_(7)  - e7];
  f8  = s[_(8)  - e8];

  }
  /*M->sY = M->lX*M->lY; M->sZ = M->lX*M->lY*M->lZ;int _mX = M->lX - 1;  int _mY = M->lY - 1;  int _mZ = M->lZ - 1;  int _eY = M->lX*Q;  int _eZ = M->sY*Q;  int _v0 = 4*M->lX, now changed to 4*(M->lX+2);  int _vY = M->lX; int _vZ = 3*M->lX;*/
  /* CL pour cavite*/
  else if (x == 0 )
  {
    f0  = s[0];
    f1 =s[_(3)];
    f5 =s[_(7)];
    f8 =s[_(6)];
    if(y == 0)
    {
      f2 = s[_(4)];
      f3  = s[_(3)  - e3];
      f4  = s[_(4)  - e4];
      f6 = s[_(8)];
      f7  = s[_(7)  - e7];
    }
    else if ( y == mY )
    {
      f2  = s[_(2)  - e2];
      f3  = s[_(3)  - e3];
      f4 = s[_(2)];
      f6  = s[_(6)  - e6];
      f7  = s[_(5)];
    }
    else
    {
      f2  = s[_(2)  - e2];
      f3  = s[_(3)  - e3];
      f4  = s[_(4)  - e4];
      f6  = s[_(6)  - e6];
      f7  = s[_(7)  - e7];
     }
   }
   else if (y == 0 && x > 0 && x < mX)
   {
     f0  = s[0];
     f1  = s[_(1)  - e1];
     f3  = s[_(3)  - e3];
     f4  = s[_(4)  - e4];
     f7  = s[_(7)  - e7];
     f8  = s[_(8)  - e8];
     f2 = s[_(4)];
     f5 = s[_(7)];
     f6 = s[_(8)];
    }
    else if (y == mY && x > 0 && x < mX)
    {
      f0  = s[0];
      f1  = s[_(1)  - e1];
      f2  = s[_(2)  - e2];
      f3  = s[_(3)  - e3];
      f5  = s[_(5)  - e5];
      f6  = s[_(6)  - e6];
      f4 = s[_(2)];
      f7 = s[_(5)]-(1./6)*(*r0)*(*ui);
      f8 = s[_(6)]+(1./6)*(*r0)*(*ui);
    }
    else if (x == mX)
    {
      f0  = s[0];
      f1  = s[_(1)  - e1];
      f3 =s[_(1)];
      f6 =s[_(8)];
      f7 =s[_(5)];
      if (y==0)
      {
        f2  = s[_(4)];
        f4  = s[_(4)  - e4];
        f5  = s[_(7)];
        f8  = s[_(8)  - e8];
      }
      else if ( y == mY )
      {
        f2  = s[_(2)  - e2];
        f5  = s[_(5)  - e5];
        f4  = s[_(2)];
        f7  = s[_(5)];
        f8  = s[_(6)] ;
       }
       else
       {
        f2  = s[_(2)  - e2];
        f4  = s[_(4)  - e4];
        f5  = s[_(5)  - e5];
        f8  = s[_(8)  - e8];
       }
      }

/*
  // Boundary conditions 1 : Bounce back, impermeable walls, constants pressure on x=0 and x=mX
  if (x == 0 )
 {
   f0  = s[0];
   f1 =s[_(3)]+(2.*1./3)*(*ui);
   f5 =s[_(7)]+(2.*1./12)*(*ui);
   f8 =s[_(6)]+(2.*1./12)*(*ui);
   if(y == 0)
   {
     f2 = s[_(4)];
     f3  = s[_(3)  - e3];
     f4  = s[_(4)  - e4];
     f6 = s[_(7)];
     f7  = s[_(7)  - e7];
   }
   else if ( y == mY )
   {
     f2  = s[_(2)  - e2];
     f3  = s[_(3)  - e3];
     f4 = s[_(2)];
     f6  = s[_(6)  - e6];
     f7  = s[_(6) ];

   }
   else
   {
     f2  = s[_(2)  - e2];
     f3  = s[_(3)  - e3];
     f4  = s[_(4)  - e4];
     f6  = s[_(6)  - e6];
     f7  = s[_(7)  - e7];
   }

 }
 if (y == 0 && x > 0 && x < mX) {
     f0  = s[0];
     f1  = s[_(1)  - e1];
     f2 = s[_(4)];
     f3  = s[_(3)  - e3];
     f4  = s[_(4)  - e4];
     f5 = s[_(8)];
     f6 = s[_(7)];
     f7  = s[_(7)  - e7];
     f8  = s[_(8)  - e8];

   }
   if (y == mY && x > 0 && x < mX) {
     f0  = s[0];
     f1  = s[_(1)  - e1];
     f2  = s[_(2)  - e2];
     f3  = s[_(3)  - e3];
     f4 = s[_(2)];
     f5  = s[_(5)  - e5];
     f6  = s[_(6)  - e6];
     f7 = s[_(6)];
     f8 = s[_(5)];

   }
   if (x == mX)
    {
     f0  = s[0];
     f1  = s[_(1)  - e1];
     f3 =s[_(3)];
     f6 =s[_(6)];
     f7 =s[_(7)];
      if(y == 0)
      {
        f2 = s[_(4)];
        f4 = s[_(4) - e4] ;
        f5  = s[_(8)];
        f8 = s[_(8) - e8] ;
      }
      else if ( y == mY )
      {
        f2 =s[_(2) - e2];
        f4 =s[_(2)];
        f5 =s[_(5) - e5];
        f8 =s[_(5)];

      }
      else
      {
        f2 =s[_(2) - e2];
        f4 = s[_(4) - e4] ;
        f5 =s[_(5) - e5] ;
        f8 =s[_(8) - e8] ;
      }
   //   dux=(*umo)*(3*v->x -4*v[-1].x +v[-2].x )/2.;
   //   duy=(*umo)*(3*v->y -4*v[-1].y +v[-2].y )/2.;
   //   f3 =s[_(3)]-(3./9)*dux;
   //   f6 =s[_(6)]+(3./36)*(-dux+duy);
   //   f7 =s[_(7)]+(3./36)*(-dux-duy);
   }
*/
  //__syncthreads();
  // if (threadIdx.x == 0) *umo2=0;
  // Computation of fequilibre
  feq0  = i0*(*r0)*(1.-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq1  = i1*(*r0)*(1+3*(v->x)+(9./2.)*((v->x)*(v->x))-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq2  = i1*(*r0)*(1+3*(v->y)+(9./2.)*((v->y)*(v->y))-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq3  = i1*(*r0)*(1-3*(v->x)+(9./2.)*((v->x)*(v->x))-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq4  = i1*(*r0)*(1-3*(v->y)+(9./2.)*((v->y)*(v->y))-(3./2.)*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq5  = i2*(*r0)*(1+3*((v->x)+(v->y))+9.*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq6  = i2*(*r0)*(1-3*((v->x)-(v->y))-9.*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq7  = i2*(*r0)*(1-3*((v->x)+(v->y))+9.*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq8  = i2*(*r0)*(1+3*((v->x)-(v->y))-9.*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );

  // Computation of moments
  rho=f0+f1+f2+f3+f4+f5+f6+f7+f8;
  e = -4.F*f0-f1-f2-f3-f4+2.F*f5+2.F*f6+2.F*f7+2.F*f8;
  eps = 4.F*f0-2.F*f1-2.F*f2-2.F*f3-2.F*f4+f5+f6+f7+f8;
  jx = f1-f3+f5-f6-f7+f8;
  qx = -2.F*f1 + 2.F*f3+f5-f6-f7+f8;
  jy = f2-f4+f5+f6-f7-f8;
  qy = -2.F*f2+2.F*f4+f5+f6-f7-f8;
  pxx = f1-f2+f3-f4;
  pxy =f5-f6+f7-f8;

  muloc=s7*s72*(2.)/(((*phi) +1.)*s72+(1.-(*phi))*s7);
  sp=1./(3*muloc/(*r0) + 0.5);
  sm=1./((Lambda/(sp-0.5))+0.5);
  // moments computations,
  rho -= 0*(rho-(feq0+feq1+feq2+feq3+feq4+feq5+feq6+feq7+feq8));
  e   -= sp*(e -(-4.F*feq0-feq1-feq2-feq3-feq4+2.F*feq5+2.F*feq6+2.F*feq7+2.F*feq8 ))+(1-sp/2.)*6*(*r0)*((Fs->x)*(v->x) + (Fs->y)*(v->y));
  eps -= sp*(eps -(4.F*feq0-2.F*feq1-2.F*feq2-2.F*feq3-2.F*feq4+feq5+feq6+feq7+feq8))-(1-sp/2.)*6*(*r0)*((Fs->x)*(v->x) + (Fs->y)*(v->y));
  qx  -= sm*(qx -(-2.F*feq1 + 2.F*feq3+feq5-feq6-feq7+feq8 ))+(1-sm/2.)*(*r0)*(-(Fs->x));
  qy  -= sm*(qy -(-2.F*feq2+2.F*feq4+feq5+feq6-feq7-feq8))+(1-sm/2.)*(*r0)*(-(Fs->y));
  pxx -= sp*(pxx -(feq1-feq2+feq3-feq4))+(1-sp/2.)*2*(*r0)*((Fs->x)*(v->x)-(Fs->y)*(v->y));
  pxy -= sp*(pxy-(feq5-feq6+feq7-feq8))+(1-sp/2.)*(*r0)*((Fs->y)*(v->x)+(Fs->x)*(v->y));
  // jx-=-(1-s7/2)*(Fs->x);
  // jy-=-(1-s7/2)*(Fs->y);
  jx-= 0*(jx-(feq1-feq3+feq5-feq6-feq7+feq8))+(1-0/2.)*(*r0)*(Fs->x);
  jy-= 0*(jy-(feq2-feq4+feq5+feq6-feq7-feq8))+(1-0/2.)*(*r0)*(Fs->y);
  // Collision and writing

  d[0]    = c0*rho - c0*e + c0*eps;
  d[_(1)] = c0*rho-c1*qx+c2*pxx+c1*jx-c3*eps-c4*e;
  d[_(2)] = c0*rho-c1*qy-c2*pxx+c1*jy-c3*eps-c4*e;
  d[_(3)] = c0*rho+c1*qx+c2*pxx-c1*jx-c3*eps-c4*e;
  d[_(4)] = c0*rho+c1*qy-c2*pxx-c1*jy-c3*eps-c4*e;
  d[_(5)] = c0*rho+c5*qy+c5*qx+c2*pxy+c1*jy+c1*jx+c4*eps+c3*e;
  d[_(6)] = c0*rho+c5*qy-c5*qx-c2*pxy+c1*jy-c1*jx+c4*eps+c3*e;
  d[_(7)] = c0*rho-c5*qy-c5*qx+c2*pxy-c1*jy-c1*jx+c4*eps+c3*e;
  d[_(8)] = c0*rho-c5*qy+c5*qx-c2*pxy-c1*jy+c1*jx+c4*eps+c3*e;

  //
  /*
  if (x == mX)
  {
    // dux=-(uinitx)*(3*v->x -4*v[-1].x +v[-2].x )/2.;
    // duy=-(uinitx)*(3*v->y -4*v[-1].y +v[-2].y )/2.;
    duy=-(*umo1)*(3*v->y -4*v[-1].y +v[-2].y )/2.;
    dux=-(*umo1)*(3*v->x -4*v[-1].x +v[-2].x )/2.;
    d[_(3)] =s[_(3)] -(3./9)*dux;
    d[_(6)] =s[_(6)] +(3./36)*(-dux+duy);
    d[_(7)] =s[_(7)] +(3./36)*(-dux-duy);
  }

  __syncthreads();
  if (threadIdx.x == 0) *umo2=0;
  */
}



/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
__global__ void lbmiD2Q9MRT(float* s, float* d, float* ui,vector* v, float* r0, vector* Fs, float* pr, int* phi)
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y +threadIdx.y;
  int offset = x + y*lX ;

  int i = index(x, y);

  v += offset;
  r0 += offset;
  s += i;
  d += i;
  Fs += offset;
  pr +=offset;
  phi += offset;
  float f0, f1, f2, f3, f4, f5, f6,f7, f8;
  float feq0,feq1,feq2,feq3,feq4,feq5,feq6,feq7,feq8;
  float m0,m1,m2,m3,m4,m5,m6,m7,m8;
  float muloc,sloc;
  // Reading and propagation

  f0  = s[0];
  f1  = s[_(1)];
  f2  = s[_(2)];
  f3  = s[_(3)];
  f4  = s[_(4)];
  f5  = s[_(5)];
  f6  = s[_(6)];
  f7  = s[_(7)];
  f8  = s[_(8)];


  feq0     = -(1. - i0)*(*pr)*3 - i0*(3./2.)*((v->x)*(v->x) + (v->y)*(v->y));
  feq1  = i1*((*pr)*3+3*(v->x)+(9./2)*((v->x)*(v->x))-(3./2.)*((v->x)*(v->x) + (v->y)*(v->y)) );
  feq2  = i1*((*pr)*3+3*(v->y)+(9./2)*((v->y)*(v->y))-(3./2.)*((v->x)*(v->x) + (v->y)*(v->y)) );
  feq3  = i1*((*pr)*3-3*(v->x)+(9./2)*((v->x)*(v->x))-(3./2.)*((v->x)*(v->x) + (v->y)*(v->y)) );
  feq4  = i1*((*pr)*3-3*(v->y)+(9./2)*((v->y)*(v->y))-(3./2.)*((v->x)*(v->x) + (v->y)*(v->y)) );
  feq5  = i2*((*pr)*3+3*((v->x)+(v->y))+(9.)*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq6  = i2*((*pr)*3+3*(-(v->x)+(v->y))-(9.)*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq7  = i2*((*pr)*3+3*(-(v->x)-(v->y))+(9.)*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq8  = i2*((*pr)*3+3*((v->x)-(v->y))-(9.)*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );

  // Computation of moments
  m0=f0+f1+f2+f3+f4+f5+f6+f7+f8;
  m1 = -4.F*f0-f1-f2-f3-f4+2.F*f5+2.F*f6+2.F*f7+2.F*f8;
  m2 = 4.F*f0-2.F*f1-2.F*f2-2.F*f3-2.F*f4+f5+f6+f7+f8;
  m3 = f1-f3+f5-f6-f7+f8;
  m4 = -2.F*f1 + 2.F*f3+f5-f6-f7+f8;
  m5 = f2-f4+f5+f6-f7-f8;
  m6 = -2.F*f2+2.F*f4+f5+f6-f7-f8;
  m7 = f1-f2+f3-f4;
  m8 =f5-f6+f7-f8;

  // moments computations,
  //Substraction of non-equilibrium terms and relaxation

  // tx = v->x;
  // ty = v->y;
  // tx2 = tx*tx;
  // ty2 = ty*ty ;
  // e   -= s1*(e -(-2*(*r0) +3.F*(*r0)*(tx2 + ty2)));
  // eps -= s2*(eps -((*r0)-3.F*(*r0)*(tx2+ty2)));
  // qx  -= s4*(qx -( -1.F*(*r0)*tx ));
  // qy  -= s4*(qy -( -1.F*(*r0)*ty));
  // pxx -= s7*(pxx -(*r0)*(tx2-ty2));
  // pxy -= s7*(pxy-(*r0)*(tx*ty));
  muloc=s7*s72*(2.)/(((*phi) +1.)*s72+(1.-(*phi))*s7);
  sloc=1./(3*muloc + 0.5);

  m0 -= (2*sloc/(2+sloc))*(m0-(feq0+feq1+feq2+feq3+feq4+feq5+feq6+feq7+feq8));
  m1 -= (2*s1/(2+s1))*(m1 -(-4.F*feq0-feq1-feq2-feq3-feq4+2.F*feq5+2.F*feq6+2.F*feq7+2.F*feq8 )); //+(1-s1/2.)*6*(*r0)*((Fs->x)*(v->x) + (Fs->y)*(v->y));
  m2 -= (2*s2/(2+s2))*(m2 -(4.F*feq0-2.F*feq1-2.F*feq2-2.F*feq3-2.F*feq4+feq5+feq6+feq7+feq8)); //-(1-s2/2.)*6*(*r0)*((Fs->x)*(v->x) + (Fs->y)*(v->y));
  m4  -= (2*s4/(2+s4))*(m4 -(-2.F*feq1 + 2.F*feq3+feq5-feq6-feq7+feq8 )); //+(1-s4/2.)*(*r0)*(-(Fs->x));
  m6  -= (2*s4/(2+s4))*(m6 -(-2.F*feq2+2.F*feq4+feq5+feq6-feq7-feq8)); //+(1-s4/2.)*(*r0)*(-(Fs->y));
  m7 -=(2*sloc/(2+sloc))*(m7 -(feq1-feq2+feq3-feq4)); //+(1-sloc/2.)*2*(*r0)*((Fs->x)*(v->x)-(Fs->y)*(v->y));
  m8 -= (2*sloc/(2+sloc))*(m8-(feq5-feq6+feq7-feq8)); //+(1-sloc/2.)*(*r0)*((Fs->y)*(v->x)+(Fs->x)*(v->y));
  // jx-=-(1-s7/2)*(Fs->x);
  // jy-=-(1-s7/2)*(Fs->y);
  m3-= (2*sloc/(2+sloc))*(m3-(feq1-feq3+feq5-feq6-feq7+feq8)); //+(1-sloc/2.)*(*r0)*(Fs->x);
  m5-= (2*sloc/(2+sloc))*(m5-(feq2-feq4+feq5+feq6-feq7-feq8)); //+(1-sloc/2.)*(*r0)*(Fs->y);

  /*
  m0 -= sloc*(m0-(feq0+feq1+feq2+feq3+feq4+feq5+feq6+feq7+feq8));
  m1 -= sloc*(m1 -(-4.F*feq0-feq1-feq2-feq3-feq4+2.F*feq5+2.F*feq6+2.F*feq7+2.F*feq8 )); //+(1-s1/2.)*6*(*r0)*((Fs->x)*(v->x) + (Fs->y)*(v->y));
  m2 -= sloc*(m2 -(4.F*feq0-2.F*feq1-2.F*feq2-2.F*feq3-2.F*feq4+feq5+feq6+feq7+feq8)); //-(1-s2/2.)*6*(*r0)*((Fs->x)*(v->x) + (Fs->y)*(v->y));
  m4  -= sloc*(m4 -(-2.F*feq1 + 2.F*feq3+feq5-feq6-feq7+feq8 )); //+(1-s4/2.)*(*r0)*(-(Fs->x));
  m6  -= sloc*(m6 -(-2.F*feq2+2.F*feq4+feq5+feq6-feq7-feq8)); //+(1-s4/2.)*(*r0)*(-(Fs->y));
  m7 -= sloc*(m7 -(feq1-feq2+feq3-feq4)); //+(1-sloc/2.)*2*(*r0)*((Fs->x)*(v->x)-(Fs->y)*(v->y));
  m8 -= sloc*(m8-(feq5-feq6+feq7-feq8)); //+(1-sloc/2.)*(*r0)*((Fs->y)*(v->x)+(Fs->x)*(v->y));
  // jx-=-(1-s7/2)*(Fs->x);
  // jy-=-(1-s7/2)*(Fs->y);
  m3-= sloc*(m3-(feq1-feq3+feq5-feq6-feq7+feq8)); //+(1-sloc/2.)*(*r0)*(Fs->x);
  m5-= sloc*(m5-(feq2-feq4+feq5+feq6-feq7-feq8)); //+(1-sloc/2.)*(*r0)*(Fs->y);
  */
  // Collision and writing

  d[0]= c0*m0 - c0*m1 + c0*m2;
  d[_(1)] = c0*m0-c1*m4+c2*m7+c1*m3-c3*m2-c4*m1;
  d[_(2)] = c0*m0-c1*m6-c2*m7+c1*m5-c3*m2-c4*m1;
  d[_(3)] = c0*m0+c1*m4+c2*m7-c1*m3-c3*m2-c4*m1;
  d[_(4)] = c0*m0+c1*m6-c2*m7-c1*m5-c3*m2-c4*m1;
  d[_(5)] = c0*m0+c5*m6+c5*m4+c2*m8+c1*m5+c1*m3+c4*m2+c3*m1;
  d[_(6)] = c0*m0+c5*m6-c5*m4-c2*m8+c1*m5-c1*m3+c4*m2+c3*m1;
  d[_(7)] = c0*m0-c5*m6-c5*m4+c2*m8-c1*m5-c1*m3+c4*m2+c3*m1;
  d[_(8)] = c0*m0-c5*m6+c5*m4-c2*m8-c1*m5+c1*m3+c4*m2+c3*m1;


}

///////////////////////////////////////////////////////////
__global__ void lbmBGKiD2Q9(float* s, float* d, float* ui, vector* v, float* r0,vector* Fs, float* pr, int* phi)
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y +threadIdx.y;
  int offset = x + y*lX ;

  int i = index(x, y);
  float sloc;
  v += offset;
  r0 += offset;
  Fs +=offset;

  pr +=offset;
  phi += offset;
  s += i;
  d += i;


  float f0, f1, f2, f3, f4, f5, f6,f7, f8;

  float feq0,feq1,feq2,feq3,feq4,feq5,feq6,feq7,feq8;
  float fx,fy;
  float tampon; //,tampon2;
  float omegabgk;
  float muloc;
  fx=(Fs->x);
  fy=(Fs->y);

  //ui += x;

  f0  = s[0];
  f1  = s[_(1)];
  f2  = s[_(2)];
  f3  = s[_(3)];
  f4  = s[_(4)];
  f5  = s[_(5)];
  f6  = s[_(6)];
  f7  = s[_(7)];
  f8  = s[_(8)];


  feq0     = -(1. - i0)*(*pr)*3 - i0*(3./2.)*((v->x)*(v->x) + (v->y)*(v->y));
  feq1  = i1*((*pr)*3+3*(v->x)+(9./2)*((v->x)*(v->x))-(3./2.)*((v->x)*(v->x) + (v->y)*(v->y)) );
  feq2  = i1*((*pr)*3+3*(v->y)+(9./2)*((v->y)*(v->y))-(3./2.)*((v->x)*(v->x) + (v->y)*(v->y)) );
  feq3  = i1*((*pr)*3-3*(v->x)+(9./2)*((v->x)*(v->x))-(3./2.)*((v->x)*(v->x) + (v->y)*(v->y)) );
  feq4  = i1*((*pr)*3-3*(v->y)+(9./2)*((v->y)*(v->y))-(3./2.)*((v->x)*(v->x) + (v->y)*(v->y)) );
  feq5  = i2*((*pr)*3+3*((v->x)+(v->y))+(9.)*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq6  = i2*((*pr)*3+3*(-(v->x)+(v->y))-(9.)*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq7  = i2*((*pr)*3+3*(-(v->x)-(v->y))+(9.)*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );
  feq8  = i2*((*pr)*3+3*((v->x)-(v->y))-(9.)*((v->x)*(v->y))+3.*((v->x)*(v->x)+(v->y)*(v->y)) );


  //Substraction of non-equilibrium terms and relaxation
  //sloc=s7*(*phi)+s72*(1-(*phi));
  muloc=s7*s72*(2.)/(((*phi) +1.)*s72+(1.-(*phi))*s7);
  sloc=1./(3*muloc/(*r0) + 0.5);
  //sloc=1./(3*muloc + 0.5);
  //((*phi) -(-1.0))/(1.0-(-1.0))*(s7-s72)+s72;
  //sloc=((*fcar) -(-1))/(1-(-1.))*(s7-s72)+s72;
  omegabgk = (1.-sloc/(2.));
  tampon =0.0;
  //tampon += (tx)*(fx);
  f0  -= sloc*(f0-feq0);
  f0  +=omegabgk*(tampon) ;
  // f0  -=(1-s7/(2))*(-4./3)*((tx)*(fx) + (ty)*(fy)) ;
  f1  -= sloc*(f1-feq1);

  tampon = i1*3*fx/(*r0);
  // tampon += (fx)*(2*(tx)/3 + 1./3);
  f1  +=omegabgk*(tampon);
  // f1  -=omegabgk*((fx)*(2*(tx)/3 + 1./3) - (ty)*(fy)/3.);
  f2  -= sloc*(f2-feq2);
  tampon = i1*3*fy/(*r0);
  // tampon += (fy)*(2*(ty)/3 + 1./3);
  f2  +=omegabgk*(tampon);
  // f2  -=omegabgk*((fy)*(2*(ty)/3 + 1./3) - (tx)*(fx)/3.);
  f3  -= sloc*(f3-feq3);
  tampon = -i1*3*fx/(*r0);
  // tampon += (fx)*(2*(tx)/3 - 1./3);
  f3  +=omegabgk*(tampon);
  // f3  -=omegabgk*((fx)*(2*(tx)/3 - 1./3) - (ty)*(fy)/3.);
  f4  -= sloc*(f4-feq4);
  tampon = - i1*3*fy/(*r0);
  // tampon += (fy)*(2*(ty)/3 - 1./3);
  f4  +=omegabgk*(tampon);
  // f4  -=omegabgk*((fy)*(2*(ty)/3 - 1./3) - (tx)*(fx)/3.);
  f5  -= sloc*(f5-feq5);
  tampon=i2*3*(fx+fy)/(*r0);
  // tampon += (fx)*((tx)/6 +(ty)/4 + 1./12);
  f5  +=omegabgk*(tampon );
  // f5  -=omegabgk*((fx)*((tx)/6 +(ty)/4 + 1./12) + (fy)*((ty)/6 +(tx)/4 + 1./12) );
  f6  -= sloc*(f6-feq6);
  tampon=i2*3*(-fx+fy)/(*r0);
  // tampon += (fx)*((tx)/6 -(ty)/4 - 1./12) ;
  f6  +=omegabgk*(tampon );
  // f6  -=omegabgk*((fx)*((tx)/6 -(ty)/4 - 1./12) + (fy)*((ty)/6 -(tx)/4 + 1./12) );
  f7  -= sloc*(f7-feq7);
  tampon=i2*3*(-fx-fy)/(*r0);
  // tampon += (fx)*((tx)/6 +(ty)/4 - 1./12);
  f7  +=omegabgk*(tampon );
  // f7  -=omegabgk*((fx)*((tx)/6 +(ty)/4 - 1./12) + (fy)*((ty)/6 +(tx)/4 - 1./12) );
  f8  -= sloc*(f8-feq8);
  tampon=i2*3*(fx-fy)/(*r0);
  // tampon += (fx)*((tx)/6 -(ty)/4 + 1./12);
  f8  +=omegabgk*(tampon);
  // f8  -=omegabgk*((fx)*((tx)/6 -(ty)/4 + 1./12) + (fy)*((ty)/6 -(tx)/4 - 1./12) );

  // Collision and writing

  d[0]= f0;
  d[_(1)] = f1;
  d[_(2)] = f2;
  d[_(3)] = f3;
  d[_(4)] = f4;
  d[_(5)] = f5;
  d[_(6)] = f6;
  d[_(7)] = f7;
  d[_(8)] = f8;
  //

}
/////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
extern "C++" void init_model(model* M)
{
  // a verifier pour block/grid
  dim3 block((M->lX)/(M->Tile_X), (M->lY)/(M->Tile_Y),1);
  dim3 grid(M->Tile_X,M->Tile_Y,1);
  M->sY = M->lX*M->lY;
  M->TP = 0;
  int _mX = M->lX - 1;    //int _mX = M->lX;
  int _mY = M->lY - 1;
  int _eY = M->lX*Q;
  /// Poids du reseau DaQb
  float _R1=M->rho;
  float _R2=M->rho2;
  float _i0 = 4./9.;
  float _i1 = 1./9.;
  float _i2 = 1./36.;
  /// coef BGK/MRT
  float _s1 = S1;
  float _s2 = S2;
  float _s4 = S4;
  float _s7 = M->nu; //1./(3*M->nu/R1+1./2.0); //   2./(6.*M->nu + 1.);
  float _s72 = M->nu2;

  float _c0  = 1./9.;
  float _c1  = 1./6.;
  float _c2  = 1./4.;
  float _c3  = 1./18.;
  float _c4  = 1./36.;
  float _c5  = 1./12.;
  cudaError_t error;
  int var;
  ////////////////////////////////////////////////////
    // Imposition vitesse en haut de la cavite
    printf("f0%f\n",M->f0);
     HOST_ALLOC(M->uinlet,M->lX,float);
     for(var=0;var<M->lX;var++)
      {
       M->uinlet[var]=M->f0;
       /*
       if(var < (M->lX)/8)
       {
         M->uinlet[var]=4.*var*(2*M->f0)/M->lX;
       }
       else if (var < 7*(M->lX)/8 )
       {
         M->uinlet[var]=(2*M->f0)/2.;
       }
       else
       {
         M->uinlet[var]=(4.*((M->lX)-1 - var)*(2*M->f0)/M->lX);
       }
       */
     }
     HOST_ALLOC(M->u0,M->sY,vector); ///M->u0 = (vector*)malloc((M->sY)*sizeof(vector*));
     if (M->u0 == NULL)
     {
       printf("Erreur allocation \n");
     }
     HOST_ALLOC(M->rh, M->sY, float);  // masse volumique
     if (M->rh == NULL)
     {
       printf("Erreur allocation \n");
     }
     HOST_ALLOC(M->lf0,M->sY,int);
     if (M->lf0 == NULL)
     {
       printf("Erreur allocation \n");
     }
     HOST_ALLOC(M->l0h,M->sY*Q,float);
     if (M->l0h == NULL)
     {
       printf("Erreur allocation \n");
     }
    /////////////////////////////////////////////////////////////
    error = DEV_ALLOC(M->ui, M->lX, float);
    if (error != cudaSuccess)
     {
         printf("cudaMalloc d_A returned error %s (code %d), line(%d)\n", cudaGetErrorString(error), error, __LINE__);
         exit(EXIT_FAILURE);
     }
    COPY_TO_DEV(M->ui, M->uinlet, M->lX, float) ;
    error = DEV_ALLOC(M->l0, M->sY*Q, float);
    if (error != cudaSuccess)
    {
      printf("cudaMalloc d_A returned error %s (code %d), line(%d)\n", cudaGetErrorString(error), error, __LINE__);
      exit(EXIT_FAILURE);
    }
    error = DEV_ALLOC(M->l1, M->sY*Q, float);
    if (error != cudaSuccess)
    {
      printf("cudaMalloc d_A returned error %s (code %d), line(%d)\n", cudaGetErrorString(error), error, __LINE__);
      exit(EXIT_FAILURE);
    }
    error = DEV_ALLOC(M->v, M->sY, vector);
    if (error != cudaSuccess)
    {
      printf("cudaMalloc d_A returned error %s (code %d), line(%d)\n", cudaGetErrorString(error), error, __LINE__);
      exit(EXIT_FAILURE);
    }
    error = DEV_ALLOC(M->r, M->sY, float);
    if (error != cudaSuccess)
    {
      printf("cudaMalloc d_A returned error %s (code %d), line(%d)\n", cudaGetErrorString(error), error, __LINE__);
      exit(EXIT_FAILURE);
    }
    error = DEV_ALLOC(M->pr, M->sY, float);
    if (error != cudaSuccess)
    {
      printf("cudaMalloc d_A returned error %s (code %d), line(%d)\n", cudaGetErrorString(error), error, __LINE__);
      exit(EXIT_FAILURE);
    }
    error = DEV_ALLOC(M->fi, M->sY, vector);
    if (error != cudaSuccess)
    {
      printf("cudaMalloc d_A returned error %s (code %d), line(%d)\n", cudaGetErrorString(error), error, __LINE__);
      exit(EXIT_FAILURE);
    }
    error = DEV_ALLOC(M->lf, M->sY, int);
    if (error != cudaSuccess)
    {
        printf("cudaMalloc d_A returned error %s (code %d), line(%d)\n", cudaGetErrorString(error), error, __LINE__);
        exit(EXIT_FAILURE);
    }
    /////////////////////////////////////////////////////////////////
  COPY_TO_CONST(Tile_X, M->Tile_X);
  COPY_TO_CONST(Tile_Y, M->Tile_Y);
  COPY_TO_CONST(lX, M->lX);
  COPY_TO_CONST(lY, M->lY);
  SET_CONST(R1);
  SET_CONST(R2);
  SET_CONST(mX);
  SET_CONST(mY);
  SET_CONST(eY);
  SET_CONST(i0);
  SET_CONST(i1);
  SET_CONST(i2);
  SET_CONST(s1);
  SET_CONST(s2);
  SET_CONST(s4);
  SET_CONST(s7);
  SET_CONST(s72);
  SET_CONST(c0);
  SET_CONST(c1);
  SET_CONST(c2);
  SET_CONST(c3);
  SET_CONST(c4);
  SET_CONST(c5);
  if ( cudaSuccess != cudaGetLastError() )
  {
    printf( "Error apres envoie data!\n" );
  }
  #ifdef MODE_C /// suite de calcul on lit le dernier resultat
      const char* filename = "stat.bin";
      FILE* fp;
      fp=fopen(filename, "r");
      if(fp != NULL)
      {
        int a = fread(&(M->TP), sizeof(int), 1, fp);
        fseek(fp, sizeof(int), SEEK_SET);
        int b = fread(M->l0h, sizeof(float), M->sY*Q, fp);
        fseek(fp, sizeof(float)*M->sY*Q+sizeof(int), SEEK_SET);
        int c = fread((M->lf0), sizeof(int), M->sY, fp);
        fclose(fp);
        if(a!=1 || b!=M->sY*Q || c!=M->sY){printf("read wrong!\n");}
        COPY_TO_DEV(M->l0, M->l0h, M->sY*Q, float);
        COPY_TO_DEV(M->lf, M->lf0, M->sY, int);
        ComputeMacroInit<<<grid, block>>>(M->l0, M->v, M->r, M->lf, M->pr);
      }
      else
      {
        // Chargement de la fonction caractéristique
        const char* filename = "fcar.bin";
        FILE* fp2;
        fp2=fopen(filename, "r");
        if(fp2 != NULL)
        {
          int c = fread((M->lf0), sizeof(int), M->sY, fp2);
          fclose(fp2);
          if( c!=M->sY){printf("read wrong! %d \n",c);}
          COPY_TO_DEV(M->lf, M->lf0, M->sY, int);
        }

        // Initialisation l0
        initUetP<<<grid, block>>>(M->v,M->pr,M->fi,M->r,M->lf);
        // Initialsitation pour D2Q9
        initD2Q9<<<grid, block>>>(M->l0, M->v, M->r);
        //--------------------------------------------------
        // Initialsitation pour iD2Q9
        //initf<<<grid, block>>>(M->l0, M->pr, M->v);
        //---------------------------------------------


      }
  #else
    // Chargement de la fonction caractéristique
    const char* filename = "fcar.bin";
    FILE* fp2;
    fp2=fopen(filename, "r");
    if(fp2 != NULL)
    {
      int c = fread((M->lf0), sizeof(int), M->sY, fp2);
      fclose(fp2);
      if( c!=M->sY){printf("read wrong! %d \n",c);}
      COPY_TO_DEV(M->lf, M->lf0, M->sY, int);
    }
    initUetP<<<grid, block>>>(M->v,M->pr,M->fi,M->r,M->lf);
    // Initialsitation pour D2Q9
    initD2Q9<<<grid, block>>>(M->l0, M->v, M->r);
    //--------------------------------------------------
    // Initialsitation pour iD2Q9
    //initf<<<grid, block>>>(M->l0, M->pr, M->v);
    //---------------------------------------------
  #endif
}
//////////////////////////////////////////////////////////////
extern "C++" void compute_tlbm(model* M)
{
  dim3 block((M->lX)/(M->Tile_X), (M->lY)/(M->Tile_Y),1);
  dim3 grid(M->Tile_X,M->Tile_Y,1);
  for (int i = 0; i < M->P; i++)
  {
  /*
  lbmBGKiD2Q9<<<grid, block>>>(M->l0, M->l1,M->ui,M->v, M->r, M->fi, M->pr, M->lf);
    //lbmiD2Q9MRT<<<grid, block>>>(M->l0, M->l1,M->ui,M->v, M->r, M->fi, M->pr, M->lf);
  Prediction<<<grid, block>>>(M->l1,M->l0,M->v,M->fi,M->r, M->lf,M->ui, M->pr);
  Correction<<<grid, block>>>(M->l0, M->v, M->pr, M->fi, M->r);
  lbmBGKiD2Q9<<<grid, block>>>(M->l0, M->l1,M->ui,M->v, M->r, M->fi, M->pr, M->lf);
  //lbmiD2Q9MRT<<<grid, block>>>(M->l0, M->l1,M->ui,M->v, M->r, M->fi, M->pr, M->lf);
  Prediction<<<grid, block>>>(M->l1,M->l0,M->v,M->fi,M->r, M->lf,M->ui, M->pr);
  Correction<<<grid, block>>>(M->l0, M->v, M->pr, M->fi, M->r);
  */

  lbmTRT<<<grid, block>>>(M->l0, M->l1,M->ui,M->v, M->r, M->fi, M->lf);
  results<<<grid, block>>>(M->l1, M->l0,M->v, M->r, M->lf,M->ui,M->fi);
  lbmTRT<<<grid, block>>>(M->l1, M->l0,M->ui,M->v, M->r, M->fi, M->lf);
  results<<<grid, block>>>(M->l0, M->l1,M->v, M->r, M->lf,M->ui,M->fi);

  }
}
//////////////////////////////////////////////////////////////
extern "C++" void compute_results(model* M)
{
  dim3 block((M->lX)/(M->Tile_X), (M->lY)/(M->Tile_Y),1);
  dim3 grid(M->Tile_X,M->Tile_Y,1);

  COPY_TO_HOST(M->u0, M->v, M->sY, vector);
  COPY_TO_HOST(M->rh, M->r, M->sY, float);
  COPY_TO_HOST(M->lf0, M->lf, M->sY, int);
  #ifdef MODE_C
  COPY_TO_HOST(M->l0h, M->l0, M->sY*Q, float);
  #endif
}
//////////////////////////////////////////////////////////////
extern "C++" void free_model(model* M)
{
  // free on memory host
  free(M->uinlet);
  free(M->u0);
  free(M->rh);
  free(M->lf0);
  free(M->l0h);



  // free memory on device
  cudaFree(M->ui);
  if ( cudaSuccess != cudaGetLastError() )
printf( "Free 1!\n" );
  cudaFree(M->l0);
  if ( cudaSuccess != cudaGetLastError() )
printf( "Free 2!\n" );
  cudaFree(M->l1);
  if ( cudaSuccess != cudaGetLastError() )
printf( "Free 3!\n" );
  cudaFree(M->v);
  if ( cudaSuccess != cudaGetLastError() )
printf( "Free 4!\n" );
  cudaFree(M->r);
  if ( cudaSuccess != cudaGetLastError() )
printf( "Free 5!\n" );
  cudaFree(M->pr);
  if ( cudaSuccess != cudaGetLastError() )
printf( "Free 6!\n" );
  cudaFree(M->fi);
  if ( cudaSuccess != cudaGetLastError() )
printf( "Free 7!\n" );
  cudaFree(M->lf);
  if ( cudaSuccess != cudaGetLastError() )
printf( "Free 8!\n" );
}
//////////////////////////////////////////////////////////////
