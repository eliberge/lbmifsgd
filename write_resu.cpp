#include <stdlib.h>
#include <stdio.h>
#include "memory.h"
#include "modelD2Q9.h"



/*################################################################ */
void ecriture_vtk(model* M,int i)
{
  FILE* file;
  char* filename;
  int lenght;
  int pas,increment;
  vector test;
  //~ printf("coucou raf %d %f %f \n", raf,x0,y0);
  HOST_ALLOC(filename, M->length + 20, char);
  //printf("%s%04d_v.vtk \n", M->name, i);
  snprintf(filename, M->length + 20, "%s%04d.vtk", M->name, i);

  file=fopen(filename,"w");

  /// Write VTK header
  fprintf(file,
    "# vtk DataFile Version 3.0\n"
    "fluid_state\n"
    "ASCII\n"
    "DATASET RECTILINEAR_GRID\n"
    "DIMENSIONS %d %d 1\n", M->lX,M->lY);
    fprintf(file,"X_COORDINATES %d float\n", M->lX);

    for(increment=0;increment<M->lX;increment++)
    {
       fprintf(file,"%.2f  ",0.+increment);
     }

     fprintf(file,"\n");
     fprintf(file,"Y_COORDINATES %d float\n", M->lY);

     for(increment=0;increment<M->lY;increment++)
     {
       fprintf(file,"%.2f ",0.+increment);
     }
     fprintf(file,"\n");
     fprintf(file,"Z_COORDINATES 1 float\n");
     fprintf(file,"0\n");
     fprintf(file,"POINT_DATA %d \n",M->sY);
     /// Write density
     fprintf(file,"SCALARS pressure float 1\n");
     fprintf(file,"LOOKUP_TABLE default\n");
     for(increment=0;increment<M->sY;increment++)
     {
       fprintf(file,"%f \n", M->rh[increment]);

     }
     fprintf(file,"SCALARS fcar float 1\n");
     fprintf(file,"LOOKUP_TABLE default\n");
     for(increment=0;increment<M->sY;increment++)
     {
       fprintf(file,"%d \n", M->lf0[increment]);

     }
     /// Write velocity
     fprintf(file,"VECTORS velocity_vector float\n");

     for(increment=0;increment<M->sY;increment++)
     {
       //printf("%f\n",M->u0->x);
       test=M->u0[increment];
       fprintf(file,"%f %f 0\n", test.x,test.y);
     }


     /// Close file
     fclose(file);
     free(filename);
}

/*################################################################ */
void write_stat2D(model* M)
{
  const char* filename = "stat.bin";
  FILE* file;
  int i = M->T*M->P*2 + M->TP;
  float* l = M->l0h;
  int* fc = M->lf0;
  file = fopen(filename, "wb");
  fwrite(&i, sizeof(int), 1, file);
  //fseek(file,sizeof(int), SEEK_SET);
  fwrite(l, sizeof(float), M->sY*Q, file);
  //fseek(file,M->sY*Q*sizeof(float)+ sizeof(int), SEEK_SET);
  fwrite(fc, sizeof(int), M->sY, file);
  fclose(file);

}
