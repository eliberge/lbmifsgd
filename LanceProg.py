
import numpy as np

# duree de la simulation en seconde
dsim=2.0
# Nombre de sauvegarde
# Attention si les sauvagerdes sont trop frequentes le calcul entre 2 sauvegardes
# peut etre plus la rapide que la sauvegarde elle meme et donc les resultats ecrits
# n importe quoi
Nsav=1

Re=5
#
uphi=0.5
Lphi=2.
muphiA=0.2
muphiB=0.2
alpha=muphiA/muphiB
rhoA=1.0
rhoB=1.0
tauA=0.75
tauB=(1/alpha)*(tauA-1/2)+1/2 # a verifier
Re=rhoA*uphi*Lphi/muphiA
Llbm=100


cs=1./np.sqrt(3)
mulbmA=(1./3)*(tauA-1./2)
mulbmB=(1./3)*(tauB-1./2)
rholbmA=1.0
rholbmB=1.0
phiA=1.
phiB=-1.

ulbm=rhoA*Re*mulbmA/Llbm
cx=1.0*Lphi/Llbm
cu=1.0*uphi/ulbm
cx=1./((tauA*1./2)*cs*cs)*muphiA/cu
ct=1.0*cx/cu


print( "Llbm=", Llbm,"mulbmA=", mulbmA)
print( "ulbm", ulbm)
print( "tauA", tauA, "tauB",tauB)
print("Re =",Re)
print("cx=",cx,"cu=",cu,"ct=",ct)
M=int(dsim/ct)

LX=Llbm
LY=Llbm
# Increment entre chaque sauvegarde
incsav=M/Nsav
print(M,incsav)

# Decomposition en block sur GPU a affiner
TileX=(LX/25)
TileY=(LY/25)

print( "LX",LX,"LY",LY,"Lx X Ly",LX*LY)
LX=25*TileX
LY=25*TileY
lx=(LX/TileX)
ly=(LY/TileY)

print( "Par block", (LX/TileX)*(LY/TileY))
print("block X", TileX,"Y", TileY,"Lx",LX,lx*TileX,"Ly",LY,ly*TileY)
X0=0.55/cx
Y0=0.5/cx
R=0.2/cx
#fcar=np.zeros((int(LX),int(LY)),dtype="int")
fcar=np.ones((int(LX),int(LY)),dtype="int")
"""
for i in range(int(LX)):
    for j in range(int(LY)):
        if (i-X0)**2 + (j-Y0)**2 <= R**2:
            fcar[j,i]=0
        else :
            fcar[j,i]=1
"""
fcarps=np.reshape(fcar,int(LX*LY))
import struct
with open('fcar.bin','wb') as fl:
    for i in range(int(LX*LY)):
        fl.write(struct.pack('i', fcarps[i]))


#incsav=2
#print './Prog re100_ %d %d  %d %d %f %f %d %d %f %f %f %f %f %f %f' %(LX,LY,Nsav,incsav, mulbm,ulbm,TileX,TileY,R,X0,Y0,masse,raideur,Xeq,Yeq)
print( './Prog Cav_ %d %d  %d %d %f %f %f %f %f %d %d' %(int(LX),int(LY),Nsav,incsav,rhoA,rhoB, mulbmA,mulbmB,ulbm,int(TileX),int(TileY)))
#u1=commands.getstatusoutput('./Prog re100_ %d %d  %d %d %f %f %d %d %f %f %f %f %f %f %f' %(LX,LY,Nsav,incsav, mulbm,ulbm,TileX,TileY,R,X0,Y0,masse,raideur,Xeq,Yeq))
# #
#print  u1
