#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "memory.h"
#include "modelD2Q9.h"


#define MAX_L 51200000
#define MAX_T 655350

void print_error_msg( const char* msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}
void get_param(int argc, char** argv, model* M,int dev)
//./thelma 1 name 128 50 100 100000000 0.005 for air convection pur, nu=0.05 for octadecane
//./thelma 0 11July_Ra107 128 500 1000 10000000 0.06 for fusion of octadecane (Valided)
//./thelma 0 Ra108_ 128 710 500 100000000 0.075 for fusion of octadecane, stable till 355000 time steps
{   // Nombre d iteration totale : TxP; On sauvegarde tout les P iterations
    if (argc < 8) print_error_msg("Missing parameters.\n");

    M->dev=dev;
    M->name = argv[1];
    M->lX = atoi(argv[2]);
    M->lY = atoi(argv[3]);
    // T = Nombre de fois ou on sauvegarde²
    M->T = atoi(argv[4]);
    // Sauvegarde toute les P
    // Si la frequence de sauvegarde est trop eleve ca ne marche pas. il essaie
    // d'ecrire alors que la sauvegarde precedente n'est pas terminee
    M->P = atoi(argv[5])/2;
    M->rho=atof(argv[6]);
    M->rho2=atof(argv[7]);
    M->nu = atof(argv[8]);
    M->nu2 = atof(argv[9]);
    M->f0 = atof(argv[10]);
    // calcul de Tile_X et Tile_Y .
    // A parametrer selon la carte graphique ?
    // je me restrein en 2D a des blocks de 32x 32 approximativement sur ordi TAKEUCHI (1024 thread par block)
    M->Tile_X=atoi(argv[11]); ;
    M->Tile_Y=atoi(argv[12]); ;
    if (M->dev < 0) print_error_msg("Wrong device number\n");
    if (M->lX <= 0 || M->lX > MAX_L) print_error_msg("Wrong lattice size.\n");
    if (M->T <= 0 || M->T > MAX_T) print_error_msg("Wrong period number.\n");
    if (M->P <= 0 || M->P > MAX_T) print_error_msg("Wrong period duration.\n");
    if (M->nu <= 0) print_error_msg("Wrong viscosity.\n");
    //    read_domain(argv[7], M);
    printf("D2Q9 MRT  on device %d\n", M->dev);
    //    M->lZ =  M->lX;
    //    M->lY = M->lZ;
    printf("Lattice size: %d x %d x \n", M->lX, M->lY);
    printf("Run case name:%s\n", M->name);
    M->length = strlen(M->name);
    //M->FileE = fopen("COEFFICIENTS", "w");
    //M->FileS = fopen("STRUCTURE", "w");
}
