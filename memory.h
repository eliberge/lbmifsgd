#ifdef _SINGLE_
typedef float real;
#endif

#ifdef _DOUBLE_
typedef double real;
#endif

#define HOST_ALLOC(x, n, t) (x) = (t*)malloc((n) * sizeof(t))

#define DEV_ALLOC(x, n, t) \
    cudaMalloc((void **)&(x), (n) * sizeof(t))

#define COPY_TO_DEV(x, y, n, t) \
    cudaMemcpy((x), (y), (n)*sizeof(t), cudaMemcpyHostToDevice)

#define COPY_TO_HOST(x, y, n, t) \
    cudaMemcpy((x), (y), (n)*sizeof(t), cudaMemcpyDeviceToHost)

#define COPY_TO_CONST(x, y) \
    cudaMemcpyToSymbol(x, (void *)(&(y)), sizeof x, 0, cudaMemcpyHostToDevice)

#define SET_CONST(x) COPY_TO_CONST(x, _##x)

typedef unsigned char byte;

typedef unsigned int word;
