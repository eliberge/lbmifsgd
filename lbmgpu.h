void init_model(model* M);

void free_model(model* M);

void compute_results(model* M);

void compute_tlbm(model* M);
